#!/usr/bin/env python

"""
vpfr-single-marker-detection
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""

import sys
from sensor_msgs.msg import Image
from vpfrsinglemarkerdetection.msg import SingleMarker
from vpfr import VisionPipeline

if __name__ == "__main__":

    if len(sys.argv) < 4:
        print(
            "usage: VpfrNode.py subscribechannel publishchannel additionalArgs"
        )
    else:
        VisionPipeline(
            "vpfrsinglemarkerdetection",
            sys.argv[1],
            Image,
            sys.argv[2],
            SingleMarker,
            sys.argv,
        )
